#!/usr/bin/env python
import json
import tornado
import decorators
from server.resource import FatPKIResource, YakPKIResource

class FatHandler(tornado.web.RequestHandler):
    def initialize(self, fat, use_client_cert):
        self.fat = fat
        self.use_client_cert = use_client_cert

    def get(self, args):
        self.request.args = args
        fatresource = FatPKIResource(self.fat, self.use_client_cert)
        content, content_type = fatresource.run(self.request)
        self.write(str(content).encode())
        self.finish()

class YakHandler(tornado.web.RequestHandler):
    def initialize(self, yak, use_client_cert):
        self.yak = yak
        self.use_client_cert = use_client_cert

    @decorators.unblock
    def get(self, args):
        self.request.args = args
        yakresource = YakPKIResource(self.yak, self.use_client_cert)
        result = yakresource.run(self.request)
        for header in result.headers.keys():
            self.set_header(header, result.headers[header])
        return json.dumps(result.content)


class StaticHandler(tornado.web.StaticFileHandler):

    def parse_url_path(self, path):
        if not path or path.endswith('/'):
            self.path += 'index.html'
        # TODO lock down permissions
        return super(StaticHandler, self).parse_url_path(path)
