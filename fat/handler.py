import os
import json
import logging
from collections import namedtuple
from string import Template

ServerContext = namedtuple('ServerContext', ['server_root', 'document_root', 'app_template'])
UserContext = namedtuple('UserContext', ['common_name', 'email'])
Request = namedtuple('Request', ['path', 'args', 'method', 'headers', 'user'])

class NoAppMetadataError(Exception):
    pass

class AppRequestHandler(object):
    def __init__(self, config):
        self.config = config
        self.logger = logging.getLogger('fat')
        self.server = ServerContext(
            config.get('fat.resources'),
            config.get('fat.document_root'),
            config.get('fat.app_template')
            )

    def handle(self, request):
        path = request.path.replace('/fat/', '').lstrip('/')
        app_dir = os.path.join(self.server.document_root, path)
        app_template_path = os.path.join(self.server.server_root, self.server.app_template)
        meta_path = os.path.join(app_dir, '.meta')
        if not os.path.exists(meta_path):
            raise NoAppMetadataError(
                'No .meta found in {0}, cannot serve directories'.format(app_dir)
                )

        metadata = json.loads(self._get_file_content(meta_path))
        app_template = Template(self._get_file_content(app_template_path))
        html_path = os.path.join(app_dir, metadata['html'])
        html_body = self._get_file_content(html_path)
        includes = self._get_include_tags(metadata['includes'])
        content = app_template.substitute(
            includes=includes,
            title=metadata['title'].upper(),
            description=metadata['description'],
            image=metadata['image'],
            html_body=html_body
            )

        return content, 'text/html'

    def _get_file_content(self, file_path):
        with open(file_path, 'r') as handle:
            return handle.read()
        return None

    def _get_include_tags(self, includes):
        css_template = '<link rel="stylesheet" href="{0}"></link>'
        js_template = '<script src="{0}"></script>'
        include_types = {'css': css_template, 'javascript': js_template}

        include_tags = []

        for include_type, template in include_types.items():
            if include_type not in includes:
                continue
            for include in includes[include_type]:
                include_tags.append(template.format(include))

        return '\n'.join(include_tags)
