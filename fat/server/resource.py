"""
YakPKIResource

Module for handling and sending requests to Yak
"""
import logging
from handler import Request, UserContext

class PKIResource(object):
    """ Extension of Twisted Resource that provides PKI Auth and a defered action """
    def __init__(self, use_client_cert=True):
        object.__init__(self)
        self.use_client_cert = use_client_cert
        self.logger = logging.getLogger('fat')

    def decode_args(self, args):
        """ Decode a dict of args as provided by Twisted

        Decodes each key/value list and normalises arrays by removing [] from keys.
        Returns all args as a list to make it possible to pass multiples of args.

        :param Twisted request args. eg. {b'k':[b'v']}
        :return Normalised args for Yak. {'k':'v'}
        """
        decoded = {}
        for key, arg_list in args.items():
            if key.endswith('[]'):
                key = key[:-2]
                decoded[key] = [v.decode() for v in arg_list]
            else:
                decoded[key] = arg_list[-1].decode()

        self.logger.debug("decode_args(): {0} to {1}".format(args, decoded))
        return decoded

    def get_user_context(self, request):
        """ Get the users cert as a UserContext for use in Fat and Yak """
        if self.use_client_cert is False:
            return None

        cert = self._get_client_cert(request)
        common_name = cert['subject']['commonName']
        #email = cert['subject']['email']
        #TODO get the email with a real cert
        return UserContext(common_name, 'testEmail')

    def _get_client_cert(self, request):
        """ If TwistedYak requested the client cert then return it, otherwise return nothing. """
        if self.use_client_cert:
            return self.format_cert(request.get_ssl_certificate())
        return None

    def format_cert(self, cert):
        output = {}
        for key in cert:
            val = cert[key]
            if not isinstance(val, tuple):
                output[key] = val
            else:
                if key not in output:
                    output[key] = {}
                for item in val:
                    output[key][item[0][0]] = item[0][1]
        return output

class FatPKIResource(PKIResource):
    """ PKIResource for calling """
    def __init__(self, fat, use_client_cert=True):
        PKIResource.__init__(self, use_client_cert)
        self.fat = fat

    def run(self, request):
        service = request.uri.split('/fat/')[1]
        service = service.split('?')[0]
        print(request.arguments)
        fat_request = Request(
            path=service,
            args=self.decode_args(request.arguments),
            method=request.method,
            headers=request.headers,
            user=self.get_user_context(request)
            )

        return self.fat.handle(fat_request)


class YakPKIResource(PKIResource):
    """ PKIResource for calling Yak through TwistedYak

        PKIResource implementation for handling and sending requests to Yak
    """
    def __init__(self, yak, use_client_cert=True, extraheaders=None):
        PKIResource.__init__(self, use_client_cert)
        if extraheaders:
            self.extraheaders = extraheaders
        else:
            self.extraheaders = []

        self.logger = logging.getLogger('fat')
        self.yak = yak

    def run(self, request):
        """ Starts Yak request processing.
        :param request:
        :return the rendered page:
        """
        self.request = request
        user_context = self._get_client_cert(self.request)
        clientip = self.request.remote_ip

        if self.use_client_cert:
            subject = user_context['subject']
        else:
            subject = ''

        # prep the service request for Yak

        service = self.request.uri.split('/yak/')[1]
        service = service.split('?')[0]

        method = self.request.method
        self.logger.debug('{0} - {1}'.format(service, method))

        args = self.decode_args(self.request.arguments)

        self.logger.info(
            "{0} {1} '{2} {3}'".format(clientip, subject, method, self.request.uri))

        try:
            return self.yak.run(service, method, user_context, args=args)

        except ImportError as exception:
            self.logger.exception(exception)
            return
