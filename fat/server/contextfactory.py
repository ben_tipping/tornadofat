"""
Twisted ContextFactory that allows for accessing the client cert when requested.
"""
import logging
import ssl
from OpenSSL import SSL

class InvalidURIException(Exception):
    """ Exception for use when URI is invalid """
    pass

class PeerCertContextFactory(object):
    """
    Twisted ContextFactory Implementation that forces the client to provide a certificate
    """
    isClient = True

    def __init__(self, config, get_client_cert):
        """
        Config param is the yak config
        """
        self.config = config
        self.logger = logging.getLogger('fat')
        self.ssl_method = SSL.TLSv1_METHOD
        self.get_client_cert = get_client_cert

    def getContext(self):
        context = ssl.SSLContext(self.ssl_method)
        context.verify_mode = ssl.CERT_REQUIRED
        context.load_cert_chain(self.config.get('ssl_certificate_file'),
                                self.config.get('ssl_privatekey_file')
                                )
        context.load_verify_locations(self.config.get('ssl_ca_bundle').encode())
        return context

    def verify(self, connection, x509, errno, depth, verify_ok):
        """
        Verify client cert

        TODO
        """
        if verify_ok:
            self.logger.info("Verification succesful")
        else:
            self.logger.info("Verification failed {0}".format(x509.subject.decode()))
        return verify_ok
