#!/usr/bin/env python
"""Functions for starting TornadoYak"""
import sys
import argparse
import logging
import tornado
import tornado.httpserver
import tornado.ioloop
import tornado.auth
import tornado.web


from yak import Yak as Core
from yak.config import Config

from handler import AppRequestHandler
from server.contextfactory import PeerCertContextFactory
from requesthandlers import FatHandler, YakHandler, StaticHandler


LOGGING_LEVELS = {
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
}


def parse_args():
    """ Handle command line args with argparse """
    parser = argparse.ArgumentParser(description='Run FatYak')
    parser.add_argument('-c', '--config', nargs=1, dest='config', default='/etc/yak/config.json')
    parser.add_argument('--use-cert', nargs=1, dest='use_cert', default='True')
    parser.add_argument('--log-level', nargs=1, dest='log_level',
                        choices=LOGGING_LEVELS.keys(), default='DEBUG')
    return parser.parse_args(sys.argv[1:])




def run():
    """
        Run FatYak
    """
    args = parse_args()

    setup_fatyak(args)
    setup_logger(args.log_level)


def setup_fatyak(args):
    """
        Instantiates resources and set to listen on port 8443.
    """
    config = Config(args.config)
    use_client_cert = args.use_cert.lower() == 'true'

    yak = Core(config)
    fat = AppRequestHandler(config)

    # for importing extra modules within service packages
    sys.path.insert(1, config.get('yak.service_path'))
    context_factory = PeerCertContextFactory(yak.config, use_client_cert)

    application = tornado.web.Application([
        (r"/fat/(.*)", FatHandler, dict(fat=fat, use_client_cert=use_client_cert)),
        (r"/static/(.*)", StaticHandler, dict(path=config.get('fat.static_directory'))),
        (r"/yak/(.*)", YakHandler, dict(yak=yak, use_client_cert=use_client_cert))
        ],
    )

    server = tornado.httpserver.HTTPServer(application, ssl_options=context_factory.getContext())
    server.bind(8443)
    server.start(0)  # Forks multiple sub-processes
    tornado.ioloop.IOLoop.instance().start()



if __name__ == '__main__':
    run()

def setup_logger(level):
    """ Get a logging object """
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setLevel(LOGGING_LEVELS[level])
    handler.setFormatter(formatter)

    for package in ['fat', 'yak']:
        logger = logging.getLogger(package)
        logger.setLevel(level)
        logger.addHandler(handler)

