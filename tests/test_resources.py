""" YakPKIResource unit tests and required dummy resources """

import unittest

from yak import Yak as Core
from yak.config import Config
from fat.server.resource import FatYakResource, YakPKIResource
from fat.server.resource import InvalidURIException
from twisted.web.resource import NoResource
from twisted.web.static import File

class TestYakPKIResource(unittest.TestCase):
    """
        YakPKIResource Tests
    """
    def setUp(self):
        config = Config('./tests/resources/config.json')
        yak = Core(config)
        self.res = YakPKIResource(yak)

