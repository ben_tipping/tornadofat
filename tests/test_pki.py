""" YakPKIResource unit tests and required dummy resources """

import unittest

from twisted.web.test.test_web import DummyRequest
from twisted.test.proto_helpers import StringTransport

from OpenSSL.crypto import X509

from yak import Yak as Core
from yak.config import Config
from fat.server.resource import PKIResource

class YakDummyRequest(DummyRequest):
    """
    Dummy Request used for testing PKIResource
    """
    def __init__(self):
        DummyRequest.__init__(self, '')
        self.transport = YakStringTransport()

class YakStringTransport(StringTransport):
    """
    Dummy transport used for testing PKIResource
    """
    def __init__(self):
        StringTransport.__init__(self)

    def getPeerCertificate(self):
        """ Dummy class that can be used for testing """
        cert = X509()
        return cert

class TestPKIResource(unittest.TestCase):
    """
        YakPKIResources Tests
    """
    def setUp(self):
        config = Config('./tests/resources/config.json')
        yak = Core(config)
        self.res = PKIResource(use_client_cert=False)
        self.res_with_cert = PKIResource(yak.logger)

    def test_get_client_cert(self):
        """ Ensure PKI Resource is getting the X509 object correctly """
        request = YakDummyRequest()
        client_cert = self.res_with_cert._get_client_cert(request)
        self.assertTrue(isinstance(client_cert, X509))

        client_cert = self.res._get_client_cert(request)
        self.assertIsNone(client_cert)

    def test_decode_args(self):
        """ Test _decode_args """
        tests = [
            [{b'k':[b'v']}, {'k':'v'}],
            [{b'k[]':[b'v']}, {'k':['v']}],
            [{b'k':[b'v', b'v1']}, {'k':'v1'}],
            [{b'k':[b'v'], b'k1':[b'"']}, {'k':'v', 'k1':'"'}]
        ]

        for test in tests:
            self.assertEqual(self.res.decode_args(test[0]), test[1])

