from setuptools import setup

setup(
    name='Fat',
    description='Tornado Webserver with PKI Authentication and allows for PKI authorisation',
    packages=['fat', 'fat.server', 'tests'],
    install_requires=['tornado', 'pyopenssl']
)
